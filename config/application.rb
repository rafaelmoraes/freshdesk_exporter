# frozen_string_literal: true

require 'logger'
require_relative 'boot'
APP_ENV = ENV['APP_ENV']

# Global helpers
module App
  class << self
    def env
      APP_ENV
    end

    def logger
      @logger ||= ::Logger.new("log/#{env}.log")
    end
  end
end

Dir["#{File.dirname(__FILE__)}/../lib/**/*.rb"].sort.each { |file| require file }
