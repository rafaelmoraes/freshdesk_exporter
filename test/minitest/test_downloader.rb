# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../../lib/downloader'

# Freshdesk report Downloader tests
class TestDownloader < Minitest::Test
  attr_reader :csv_string, :workdir, :url, :settings, :http_client

  def setup
    @csv_string = 'header1,header2,header3\ncolumn1,column2,cloumn3'

    @workdir = '/tmp'
    @url = 'https://link_to_download_the_file.com'

    @http_client = Minitest::Mock.new
    @settings = Minitest::Mock.new

    settings.expect(:workdir_path, workdir)
    http_client.expect(:get, csv_string, [url])
  end

  def test_that_download_the_file_successfully
    file_name = "fake_freshdesk_export_#{Time.now.to_i}.csv"
    client = Downloader.new(settings: settings, http_client: http_client)

    client.harvest(url: url, file_name: file_name)

    assert_path_exists("#{workdir}/#{file_name}")
  end

  def test_that_the_file_is_not_download_twice
    file_name = "fake_freshdesk_export_#{Time.now.to_i}.csv"
    client = Downloader.new(settings: settings, http_client: http_client)

    client.stub(:harvested?, true) do
      assert(client.harvest(url: url, file_name: file_name))
    end
  end

  def test_that_logs_when_file_already_exist
    file_name = "existent_file_#{Time.now.to_i}.csv"
    client = Downloader.new(settings: settings, http_client: http_client)

    logger = Minitest::Mock.new
    logger.expect(:info, true, ["[Downloader] File already exist: #{workdir}/#{file_name}"])

    App.stub(:logger, logger) do
      File.stub(:exist?, true) do
        assert(client.harvest(url: url, file_name: file_name))
      end
    end
  end

  def test_that_logs_when_the_file_is_downloaded
    file_name = "fake_freshdesk_export_#{Time.now.to_i}.csv"
    client = Downloader.new(settings: settings, http_client: http_client)

    logger = Minitest::Mock.new
    logger.expect(:info, true, ["[Downloader] Download file: #{file_name}"])
    logger.expect(:info, true, ["[Downloader] File downloaded: #{workdir}/#{file_name}"])

    App.stub(:logger, logger) do
      client.stub(:harvested?, false) do
        assert(client.harvest(url: url, file_name: file_name))
      end
    end
  end
end
