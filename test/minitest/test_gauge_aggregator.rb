# frozen_string_literal: true

require 'webmock/minitest'

require_relative 'test_helper'
require_relative '../../lib/gauge_aggregator'

# Gauge aggregator unit tests
class TestGaugeAggregator < Minitest::Test
  attr_reader :result

  def setup
    parser_instance = Minitest::Mock.new
    return_it = [['ç a', 'd'], %w[col1 col2]]
    expected_params = ['my/path/file.ext']
    parser_instance.expect(:parse, return_it, expected_params)

    parser_class = Minitest::Mock.new
    parser_class.expect(:new, parser_instance)

    @result = GaugeAggregator.new.call('my/path/file.ext', parser: parser_class)
  end

  def test_that_result_has_two_items
    assert(result.size, 2)
  end

  def test_that_the_label_was_built_successfully
    data = result.first

    assert(data['ç_a'])
  end

  def test_that_the_value_was_extracted_successfully
    data = result.last

    assert(data['d'], 'col2')
  end

  def test_that_the_label_is_labelizied
    label = GaugeAggregator.new.send(:labelize, 'Ursos São Cães Gigantes')

    assert(label, 'ursos_são_cães_gigantes')
  end
end
