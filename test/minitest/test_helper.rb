# frozen_string_literal: true

ENV['APP_ENV'] ||= 'test'
require_relative '../../config/application'
require 'pry'
require 'amazing_print'

require 'minitest/autorun'
