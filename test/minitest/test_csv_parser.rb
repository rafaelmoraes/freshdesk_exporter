# frozen_string_literal: true

require 'webmock/minitest'

require_relative 'test_helper'
require_relative '../../lib/csv_parser'

# CSV parser tests
class TestCSVParser < Minitest::Test
  def test_that_the_csv_is_converted_to_array
    file_path = File.expand_path('test/fixtures/export_sample.csv')

    assert_instance_of(Array, CSVParser.new.parse(file_path))
  end
end
