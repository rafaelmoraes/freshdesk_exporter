# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../../lib/settings'

# Settings tests
class TestSettings < Minitest::Test
  def test_that_the_freshdesk_scheduled_export_url_was_informed
    assert_raises(RuntimeError) { Settings.url }
  end

  def test_that_returns_the_freshdesk_scheduled_export_url
    ENV['FRESHDESK_SCHEDULED_EXPORT_URL'] = 'https://google.com'

    assert(Settings.url, 'https://google.com')

    ENV['FRESHDESK_SCHEDULED_EXPORT_URL'] = nil
  end

  def test_that_the_freshdesk_username_was_informed
    assert_raises(RuntimeError) { Settings.username }
  end

  def test_that_returns_the_freshdesk_username
    ENV['FRESHDESK_USERNAME'] = 'zezim@email.com'

    assert(Settings.username, 'zezim@email.com')

    ENV['FRESHDESK_USERNAME'] = nil
  end

  def test_that_the_freshdesk_password_was_informed
    assert_raises(RuntimeError) { Settings.password }
  end

  def test_that_returns_the_freshdesk_password
    ENV['FRESHDESK_PASSWORD'] = 'mystery'

    assert(Settings.password, 'mystery')

    ENV['FRESHDESK_PASSWORD'] = nil
  end

  def test_that_returns_the_prometheus_default_port
    assert(Settings.metrics_port, Settings::DEFAULT_PROMETHEUS_METRICS_PORT)
  end

  def test_that_returns_the_prometheus_port
    ENV['PROMETHEUS_METRICS_PORT'] = '6000'

    assert(Settings.metrics_port, '6000')

    ENV['PROMETHEUS_METRICS_PORT'] = nil
  end

  def test_that_returns_the_prometheus_default_metrics_url
    assert(Settings.metrics_url, Settings::DEFAULT_PROMETHEUS_METRICS_URL)
  end

  def test_that_returns_the_prometheus_metrics_url
    ENV['PROMETHEUS_METRICS_URL'] = '/my_metrics'

    assert(Settings.metrics_url, 'my_metrics')

    ENV['PROMETHEUS_METRICS_URL'] = nil
  end

  def test_that_returns_the_default_workdir_path
    assert(Settings.workdir_path, Settings::DEFAULT_WORKDIR_PATH)
  end

  def test_that_returns_the_workdir_path
    ENV['WORKDIR_PATH'] = '/tmp/my/exclusive/workdir'

    assert(Settings.workdir_path, '/tmp/my/exclusive/workdir')

    ENV['WORKDIR_PATH'] = nil
  end

  def test_that_the_workdir_path_was_informed
    ENV['WORKDIR_PATH'] = ''

    assert_raises(RuntimeError) { Settings.workdir_path }

    ENV['WORKDIR_PATH'] = nil
  end

  def test_that_the_workdir_exist
    ENV['WORKDIR_PATH'] = '/tmp/freshdesk_exporter/workdir'

    Settings.workdir_path

    assert(Dir.exist?('/tmp/freshdesk_exporter/workdir'))

    ENV['WORKDIR_PATH'] = nil
  end

  def test_that_the_csv_parser_options_is_available
    assert(Settings.csv_parser_options, {})
  end
end
