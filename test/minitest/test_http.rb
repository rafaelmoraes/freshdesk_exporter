# frozen_string_literal: true

require 'webmock/minitest'

require_relative 'test_helper'
require_relative '../../lib/http'

# HTTP client tests
class TestSettings < Minitest::Test
  def test_fetch_the_link_successfully
    stub_request(:get, 'http://www.google.com').to_return(body: 'Found')

    assert(HTTP.get('http://www.google.com'), 'Found')
  end

  def test_request_logging
    stub_request(:get, 'http://www.google.com').to_return(body: 'Found')
    logger = mock_logger(password: 123)

    App.stub(:logger, logger) do
      assert(HTTP.get('http://www.google.com', user: 'zezim', pass: '123'), 'Found')
    end
  end

  def test_not_found_the_link
    stub_request(:get, 'http://www.google.com').to_return(body: '', status: 404)

    assert_raises(RuntimeError) { HTTP.get('http://www.google.com') }
  end

  def test_that_does_not_log_password_when_not_dev_or_test_mode
    stub_request(:get, 'http://www.google.com').to_return(body: 'Found')

    logger = mock_logger(password: '******')

    App.stub(:logger, logger) do
      App.stub(:env, 'not_dev_or_test') do
        assert(HTTP.get('http://www.google.com', user: 'zezim', pass: '123'), 'Found')
      end
    end
  end

  def mock_logger(password:)
    logger = Minitest::Mock.new
    logger.expect(:info, true, ['[HTTP][GET][REQUEST] url: http://www.google.com'])
    logger.expect(:info, true, ['[HTTP][GET][REQUEST] username: zezim'])
    logger.expect(:info, true, ["[HTTP][GET][REQUEST] password: #{password}"])

    logger.expect(:info, true, ['[HTTP][GET][RESPONSE] status: 200'])
    logger.expect(:info, true, ['[HTTP][GET][RESPONSE] body: Found'])

    logger
  end
end
