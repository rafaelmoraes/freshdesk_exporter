# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../../lib/freshdesk_client'

# Freshdesk Client tests
class TestFreshdeskClient < Minitest::Test
  attr_reader :json, :json_as_hash

  def setup
    @json_as_hash = {
      'export' => {
        'created_at' => '2020-09-29T14:00',
        'url' => 'https://awesome.csv'
      }
    }

    @json = @json_as_hash.to_json
  end

  def mock_settings(expectations = 1)
    settings_mock = Minitest::Mock.new

    expectations.times do
      settings_mock.expect(:url, 'https://fake.site')
      settings_mock.expect(:username, 'fakeuser')
      settings_mock.expect(:password, 'fakepassword')
    end

    settings_mock
  end

  def test_ssl_fallback
    mock = mock_ssl_fallback_context
    settings = mock[:settings]
    http_client = mock[:http_client]

    client = FreshdeskClient.new(settings: settings, http_client: http_client)

    assert_nil(client.fetch)
  end

  def test_request_logging
    mock = mock_ssl_fallback_context
    settings = mock[:settings]
    http_client = mock[:http_client]
    logger = Minitest::Mock.new
    logger.expect(:info, true, ["[#{FreshdeskClient}][Requesting without SSL]"])

    client = FreshdeskClient.new(settings: settings, http_client: http_client)

    App.stub(:logger, logger) { assert_nil(client.fetch) }
  end

  def test_that_fetch_the_data_successfully
    http_client = Minitest::Mock.new
    stg = mock_settings(2)

    http_client.expect(:get, json, [stg.url, { user: stg.username, pass: stg.password }])

    client = FreshdeskClient.new(settings: stg, http_client: http_client)

    result = client.fetch

    assert(result, json_as_hash['export'])
  end

  def mock_ssl_fallback_context
    http_client = Minitest::Mock.new
    settings = mock_settings(3)

    user = settings.username
    pass = settings.password
    url = settings.url
    url_no_https = url.gsub(/\Ahttps/, 'http')

    http_client.expect(:get, nil) { |_url| raise OpenSSL::SSL::SSLError }

    http_client.expect(:get, '{}', [url_no_https, { user: user, pass: pass }])

    { http_client: http_client, settings: settings }
  end
end
