# frozen_string_literal: true

# Normalizes gauges data
class GaugeAggregator
  attr_reader :settings

  def initialize(settings: Settings)
    @settings = settings
  end

  def call(file_path, parser: CSVParser)
    array = parser.new.parse(file_path)

    labels = extract_labels(array)
    values = extract_values(array)

    build_data(labels, values)
  end

  private

  def build_data(labels, values)
    values.each_with_object([]) do |value, memo|
      data = {}
      labels.each_with_index { |label, index| data[label] = value[index] }
      memo << data
      memo
    end
  end

  def labelize(string)
    string.downcase.gsub(' ', '_')
  end

  def extract_values(array)
    array[1..(array.size - 1)]
  end

  def extract_labels(array)
    array[0].map { |label| labelize(label) }
  end
end
