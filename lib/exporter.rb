# frozen_string_literal: true

require 'prometheus_exporter'
require 'prometheus_exporter/server'
require 'prometheus_exporter/client'
require 'prometheus_exporter/instrumentation'

# Prometheus exporter
class Exporter
  class << self
    attr_reader :server, :client, :settings, :gauge_observer

    def export!(tickets)
      tickets.each_with_index { |ticket, index| gauge_observer.observe(index + 1, ticket) }
    end

    def start!(settings: Settings)
      @settings = settings

      start_server
      create_client
      create_observers
    end

    def start_server
      return server if server

      @server = PrometheusExporter::Server::WebServer.new(
        bind: 'localhost',
        port: settings.metrics_port
      )
      server.start
    end

    def create_client
      return client if client

      local_client = PrometheusExporter::LocalClient.new(collector: server.collector)
      @client = PrometheusExporter::Client.default = local_client
    end

    def create_observers
      return gauge_observer if gauge_observer

      PrometheusExporter::Instrumentation::Process.start(
        type: 'Freshdesk',
        labels: { title: 'Tickets exportados' }
      )

      @gauge_observer = PrometheusExporter::Metric::Gauge.new('ticket', 'Dados do ticket')
      server.collector.register_metric(gauge_observer)
    end
  end
end
