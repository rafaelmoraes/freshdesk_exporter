# frozen_string_literal: true

require 'csv'

# Converts a CSV file into an Array
class CSVParser
  attr_reader :options

  def initialize(options: Settings.csv_parser_options)
    @options = options
  end

  def parse(file_path)
    CSV.read(file_path, **options).to_a
  end
end
