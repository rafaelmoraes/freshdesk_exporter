# frozen_string_literal: true

require 'fileutils'

# Global settings
module Settings
  DEFAULT_PROMETHEUS_METRICS_PORT = 5000
  DEFAULT_PROMETHEUS_METRICS_URL = '/metrics'
  DEFAULT_WORKDIR_PATH = '/tmp/freshdesk_exporter'

  class << self
    def url
      url = ENV['FRESHDESK_SCHEDULED_EXPORT_URL']

      raise 'You must inform the Freshdesk scheduled export URL' if url.nil?

      url
    end

    def username
      username = ENV['FRESHDESK_USERNAME']

      raise 'You must inform the Freshdesk username/email' if username.nil?

      username
    end

    def password
      password = ENV['FRESHDESK_PASSWORD']

      raise 'You must inform the Freshdesk password' if password.nil?

      password
    end

    def metrics_port
      metrics_port = ENV['PROMETHEUS_METRICS_PORT'] || DEFAULT_PROMETHEUS_METRICS_PORT

      raise 'You must inform the prometheus metrics port' if metrics_port.nil?

      metrics_port
    end

    def metrics_url
      metrics_url = ENV['PROMETHEUS_METRICS_URL'] || DEFAULT_PROMETHEUS_METRICS_URL

      raise 'You must inform the prometheus metrics url' if metrics_url.nil?

      metrics_url
    end

    def workdir_path
      workdir_path = ENV['WORKDIR_PATH'] || DEFAULT_WORKDIR_PATH

      raise 'You must inform the workdir path' if workdir_path.empty?

      FileUtils.mkdir_p(workdir_path)

      workdir_path
    end

    def csv_parser_options
      {}
    end
  end
end
