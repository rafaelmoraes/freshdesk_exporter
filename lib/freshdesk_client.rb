# frozen_string_literal: true

require 'json'
require_relative 'http'
require_relative 'settings'

# Freshdesk API Client
class FreshdeskClient
  attr_reader :settings, :http_client

  def initialize(settings: Settings, http_client: HTTP)
    @settings = settings
    @http_client = http_client
  end

  def fetch(parser: JSON)
    url = settings.url
    payload = try_get(url)

    parser.parse(payload)['export']
  end

  private

  def no_ssl(url)
    url.gsub(/\Ahttps/, 'http')
  end

  def try_get(url)
    get(url)
  rescue OpenSSL::SSL::SSLError # freshdesk https does not work well
    App.logger.info("[#{self.class}][Requesting without SSL]")
    get(no_ssl(url))
  end

  def get(url)
    http_client.get(url, user: settings.username, pass: settings.password)
  end
end
