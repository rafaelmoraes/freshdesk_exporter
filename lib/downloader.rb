# frozen_string_literal: true

# Gets the CSV file from S3 when needed
class Downloader
  attr_reader :workdir_path, :http_client

  def initialize(settings: Settings, http_client: HTTP)
    @workdir_path = settings.workdir_path
    @http_client = http_client
  end

  def harvest(url:, file_name:)
    return true if harvested?(file_name)

    download(url, file_name)
  end

  private

  def download(url, file_name)
    log("Download file: #{file_name}")
    str_csv = http_client.get(url)

    result = File.open(file_path(file_name), 'w') { |f| f.write(str_csv) }
    log("File downloaded: #{file_path(file_name)}")

    result
  end

  def harvested?(file_name)
    harvested = File.exist?(file_path(file_name))
    log("File already exist: #{file_path(file_name)}") if harvested

    harvested
  end

  def file_path(file_name)
    "#{workdir_path}/#{file_name}"
  end

  def log(msg)
    App.logger.info("[#{self.class}] #{msg}")
  end
end
