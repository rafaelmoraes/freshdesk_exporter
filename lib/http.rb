# frozen_string_literal: true

require 'net/http'
require 'uri'

# Generic HTTP client
class HTTP
  attr_reader :client, :url_parser

  def initialize(client = Net::HTTP, url_parser = URI)
    @client = client
    @url_parser = url_parser
  end

  def get(url, user: nil, pass: nil, max_redirect: 10, request_builder: Net::HTTP::Get)
    url = url_parser.parse(url)

    http = client.new(url.host, url.port)
    http.use_ssl = (url.scheme == 'https')

    request = request_builder.new(url)

    request.basic_auth(user, pass) if user && pass

    log_request('GET', url, user, pass)
    response = http.request(request)

    log_response('GET', response)
    parse_response(response, max_redirect)
  end

  def parse_response(response, max_redirect)
    case response
    when Net::HTTPSuccess
      response.body
    when Net::HTTPRedirection
      follow_redirect(response['location'], max_redirect)
    else
      raise "#{response['location']} - #{response.inspect}"
    end
  end

  def follow_redirect(location, max_redirect)
    raise 'Max redirect reached' if max_redirect.zero?

    warn "redirected to #{location}"
    get(location, max_redirect: max_redirect - 1)
  end

  def self.get(url, user: nil, pass: nil, max_redirect: 10)
    new.get(url, user: user, pass: pass, max_redirect: max_redirect)
  end

  private

  def log_request(verb, url, user = nil, pass = nil)
    log("[#{verb}][REQUEST] url: #{url}")
    log("[#{verb}][REQUEST] username: #{user}")
    password = %w[development test].include?(App.env) ? pass : '******'
    log("[#{verb}][REQUEST] password: #{password}")
  end

  def log_response(verb, response)
    log("[#{verb}][RESPONSE] status: #{response.code}")
    log("[#{verb}][RESPONSE] body: #{response.body}")
  end

  def log(msg)
    App.logger.info("[#{self.class}]#{msg}")
  end
end
